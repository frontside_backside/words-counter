module.exports = [
  {
    short: " - ",
    long: " "
  },
  {
    short: "i'm",
    long: "i am"
  },
  {
    short: "he's",
    long: "he is"
  },
  {
    short: "she's",
    long: "she is"
  },
  {
    short: "it's",
    long: "it is"
  },
  {
    short: "you're",
    long: "you are"
  },
  {
    short: "we're",
    long: "we are"
  },
  {
    short: "they're",
    long: "they are"
  },
  {
    short: "isn't",
    long: "is not"
  },
  {
    short: "aren't",
    long: "are not"
  },
  {
    short: "wasn't",
    long: "was not"
  },
  {
    short: "weren't",
    long: "were not"
  },
  {
    short: "don't",
    long: "do not"
  },
  {
    short: "doesn't",
    long: "does not"
  },
  {
    short: "didn't",
    long: "did not"
  },
  {
    short: "i've",
    long: "i have"
  },
  {
    short: "you've",
    long: "you have"
  },
  {
    short: "we've",
    long: "we have"
  },
  {
    short: "they've",
    long: "they have"
  },
  {
    short: "haven't",
    long: "have not"
  },
  {
    short: "hasn't",
    long: "has not"
  },
  {
    short: "hadn't",
    long: "had not"
  },
  {
    short: "i'd",
    long: "i had"
  },
  {
    short: "he'd",
    long: "he had"
  },
  {
    short: "she'd",
    long: "she had"
  },
  {
    short: "it'd",
    long: "it had"
  },
  {
    short: "you'd",
    long: "you had"
  },
  {
    short: "we'd",
    long: "we had"
  },
  {
    short: "they'd",
    long: "they had"
  },
  {
    short: "can't",
    long: "cannot"
  },
  {
    short: "couldn't",
    long: "could not"
  },
  {
    short: "mustn't",
    long: "must not"
  },
  {
    short: "shan't",
    long: "shall not"
  },
  {
    short: "shouldn't",
    long: "should not"
  },
  {
    short: "i'll",
    long: "i will"
  },
  {
    short: "he'll",
    long: "he will"
  },
  {
    short: "she'll",
    long: "she will"
  },
  {
    short: "it'll",
    long: "it will"
  },
  {
    short: "you'll",
    long: "you will"
  },
  {
    short: "they'll",
    long: "they will"
  },
  {
    short: "won't",
    long: "will not"
  },
  {
    short: "wouldn't",
    long: "would not"
  },
  {
    short: "let's",
    long: "let us"
  },
];