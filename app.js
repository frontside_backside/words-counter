const path = require('path');
const fs = require('fs');

const abbr = require('./abbr');

function parseText(text){
  text = text.toLowerCase();
  abbr.forEach(function(el){
    var expression = el.short;
    var re = new RegExp(expression, "g");
    text = text.replace(re, el.long);
  });

  text = text.replace(/\!/g,' ');
  text = text.replace(/\,/g,' ');
  text = text.replace(/\./g,' ');
  text = text.replace(/\?/g,' ');
  text = text.replace(/\s+/g, ' ');
  text = text.replace(/\s$/g, '');

  const res = text.split(' ');
  return res;
}

function countWords(data) {
  let dataLenght = data.length;
  data = data.sort();
  const res = [];

  let count = 1;
  for (let index = 1; index <= dataLenght; index++) {
    console.log(data[index]);
    const prevIndex = index - 1;
    const element = data[index];
    const prev = data[prevIndex];
    
    if (element === prev){
      count++;
    } else {
      res.push({
        value: prev,
        count: count
      });
      count = 1;
    }
    
  }

  return res;
}

fs.readFile('./DATA/text.txt', 'utf8', (err, data)=>{
  let res = parseText(data);
  res = countWords(res);
  res.sort((a, b)=>{
    return b.count - a.count;
  });
  fs.writeFile('./DATA/result.json', JSON.stringify(res), (err, data)=>{
    console.log('Complete.');
  });
  console.log(res);
});

console.log('App is running...');